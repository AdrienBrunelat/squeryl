'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function () {


  it('should automatically redirect to /logs when location hash/fragment is empty', function () {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/logs");
  });


  describe('logs', function () {

    beforeEach(function () {
      browser.get('index.html#!/logs');
    });


    it('should render logs view when user navigates to /logs', function () {
      expect(element(by.css('[ng-view] logs'))).toBeDefined();
    });

    it('should navigate to /queries when log menu button is clicked', function () {
      element.all(by.css('.menu li')).get(1).click();
      expect(browser.getLocationAbsUrl()).toMatch("/queries");
    });

  });


  describe('queries', function () {

    beforeEach(function () {
      browser.get('index.html#!/queries');
    });

    it('should render queries when user navigates to /queries', function () {
      expect(element(by.css('[ng-view] queries'))).toBeDefined();
    });

    it('should navigate to /logs when log menu button is clicked', function () {
      element.all(by.css('.menu li')).first().click();
      expect(browser.getLocationAbsUrl()).toMatch("/logs");
    });

    it('should display the query details if clicked', function () {
      var until = protractor.ExpectedConditions;
      browser.wait(until.presenceOf($('.query-row')), 5000);

      element.all(by.css('.query-row a')).first().click();
      expect(element.all(by.css('.query-table-wrapper')).first().isDisplayed()).toBeTruthy();
    });
  });
});
