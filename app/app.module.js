/**
 * Main module of the application
 *
 * @author Adrien Brunelat [adrien.brunelat@gmail.com]
 * @since 24/06/17
 */
'use strict';

// Declare app level module which depends on views, and components
angular.module('squerylApp', [
  'ngRoute',
  'squerylApp.version'
]);
