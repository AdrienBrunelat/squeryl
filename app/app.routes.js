/**
 * Mapping of the URL with the components.
 *
 * @author Adrien Brunelat [adrien.brunelat@gmail.com]
 * @since 24/06/17
 */
'use strict';

angular.module('squerylApp')

.config(['$locationProvider', '$routeProvider',
  function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider
    .when('/queries', {
      template: '<queries></queries>'
    })
    .when('/logs', {
      template: '<logs></logs>'
    })
    .otherwise({redirectTo: '/logs'});
  }]);
