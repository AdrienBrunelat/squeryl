/**
 * Component used to display the queries list.
 *
 * @author Adrien Brunelat [adrien.brunelat@gmail.com]
 * @since 24/06/17
 */
'use strict';

angular.module('squerylApp')

.component('queries', {
  templateUrl: 'components/queries/queries.component.html',
  controller: 'QueriesCtrl'
})

.controller('QueriesCtrl', ['$scope', 'queryService', function ($scope, queryService) {
  $scope.queries = [];

  queryService.getAll().then(
    function success(response) {
      response.forEach(function (query) {
        $scope.queries.push(query);
      });
    },
    function error(response) {
      // Eventually code a behavior corresponding the error case here
    }
  );

  $scope.reveal = function (query) {
    query.shown = !query.shown;
  };
}]);