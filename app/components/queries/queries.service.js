/**
 * Service managing HTTP requests for the SQL queries.
 *
 * @author Adrien Brunelat [adrien.brunelat@gmail.com]
 * @since 24/06/17
 */
'use strict';

angular.module('squerylApp')
.factory('queryService', ['$http', '$q', function ($http, $q) {
  return {
    /**
     * Returns all the SQL queries
     *
     * @returns {Promise} a HTTP promise with the list of all the queries
     */
    getAll: function () {
      var queries = [
        {
          sql: 'SELECT * FROM TABLE_0;',
          result: {
            columns: ['id', 'name', 'surname'],
            lines: [
              ['0', 'adrien', 'brunelat'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour'],
              ['1', 'jean', 'delatour']
            ]
          }
        },
        {
          sql: 'SELECT * FROM TABLE_1;',
          result: {
            columns: ['id', 'color', 'times'],
            lines: [['0', 'red', '3'], ['1', 'orange', '1']]
          }
        }
      ];

      // Since there is no backend to this dev, we fake the call's return after 250ms
      return $q(function(resolve) {
        setTimeout(function() {
            resolve(queries);
        }, 250);
      });

      // return $http.get('http://localhost:8080/sql/queries/get');
    },

    /**
     * Returns a page of SQL queries
     *
     * @param page the page number to retrieve
     * @returns {HttpPromise} a HTTP promise with the page of queries
     */
    getPage: function (page) {
      return $http.get('http://localhost:8080/sql/queries/page');
    }
  };
}]);