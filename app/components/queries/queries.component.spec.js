'use strict';

describe('queries component', function () {
  var controller, $scope, $q, deferred;

  beforeEach(module('squerylApp'));

  describe('queries controller', function () {
    beforeEach(inject(function ($controller, _$rootScope_, _$q_, queryService) {
      $scope = _$rootScope_.$new(true);
      $q = _$q_;

      // Spy on the service to avoid a real call and pass any value
      deferred = _$q_.defer();
      spyOn(queryService, 'getAll').and.returnValue(deferred.promise);

      // Init the controller
      controller = $controller('QueriesCtrl', {
        $scope: $scope,
        queryService: queryService
      });
    }));

    it('should create the controller properly', function () {
      expect(controller).toBeDefined();
    });

    it('should call the query service for the list of queries and display them', function () {
      // Resolve the service promise
      deferred.resolve([
        {
          sql: 'SELECT * FROM TABLE_0;',
          result: {
            columns: ['id', 'name', 'surname'],
            lines: [['0', 'val', 'val'], ['1', 'val', 'val']]
          }
        },
        {
          sql: 'SELECT * FROM TABLE_4;',
          result: {
            columns: ['id', 'col0', 'col1', 'col2'],
            lines: [['0', 'val', 'val', 'val'], ['1', 'val', 'val', 'val']]
          }
        }
      ]);

      $scope.$apply();

      // Check for the list of queries
      expect($scope.queries.length).toBe(2);
    });
  });
});
