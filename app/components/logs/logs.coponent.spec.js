'use strict';

describe('logs component', function () {
  var controller, $scope, $q, deferred;

  beforeEach(module('squerylApp'));

  describe('logs controller', function () {
    beforeEach(inject(function ($controller, _$rootScope_, _$q_, logService) {
      $scope = _$rootScope_.$new(true);
      $q = _$q_;

      // Spy on the service to avoid a real call and pass any value
      deferred = _$q_.defer();
      spyOn(logService, 'getAll').and.returnValue(deferred.promise);

      // Init the controller
      controller = $controller('LogsCtrl', {
        $scope: $scope,
        logService: logService
      });
    }));

    it('should create the controller properly', function () {
      expect(controller).toBeDefined();
    });

    it('should call the log service for the list of log entries and display them', function () {
      // Resolve the service promise
      deferred.resolve([
        {
          requestType: 'GET',
          content: 'log0'
        },
        {
          requestType: 'GET',
          content: 'log1'
        },
        {
          requestType: 'POST',
          content: 'log2'
        },
        {
          requestType: 'PUT',
          content: 'log3'
        }
      ]);

      $scope.$apply();

      // Check for the list of log entries
      expect($scope.logs.length).toBe(4);
    });

    it('should only display non filtered HTTP methods logs', function () {
      $scope.httpMethods.GET.included = false;

      // Resolve the service promise
      deferred.resolve([
        {
          requestType: 'GET',
          content: 'log0'
        },
        {
          requestType: 'GET',
          content: 'log1'
        },
        {
          requestType: 'POST',
          content: 'log2'
        }
      ]);

      $scope.$apply();

      // Check for the list of log entries
      expect($scope.logs.length).toBe(1);
    });
  });
});
