/**
 * Service managing HTTP requests for the logs.
 *
 * @author Adrien Brunelat [adrien.brunelat@gmail.com]
 * @since 24/06/17
 */
'use strict';

angular.module('squerylApp')
.factory('logService', ['$http', '$q', function ($http, $q) {
  return {
    /**
     * Returns all the log entries
     *
     * @returns {Promise} a HTTP promise with the list of all the log entries
     */
    getAll: function () {
      var logs = [
        {
          requestType: 'GET',
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend rhoncus aliquam. Sed eget bibendum eros. Phasellus mollis bibendum odio, eleifend pharetra nisl facilisis nec'
        },
        {
          requestType: 'GET',
          content: 'Etiam auctor, tellus nec ultrices cursus, ante est tristique nibh, a placerat diam lectus quis lectus.'
        },
        {
          requestType: 'POST',
          content: 'Proin bibendum lobortis neque nec commodo. Duis massa metus, malesuada eu suscipit faucibus, blandit ac sapien. Nam ultrices efficitur nibh vitae placerat. Sed rhoncus tortor sit amet neque laoreet euismod.'
        },
        {
          requestType: 'PUT',
          content: 'Integer porta enim odio, imperdiet ornare diam facilisis id. Suspendisse molestie augue purus, id viverra lectus fringilla a. Mauris eu eros auctor, ultricies neque non, volutpat urna. Mauris ut felis elementum, eleifend ligula vitae, dapibus sem.'
        }
      ];

      // Since there is no backend to this dev, we fake the call's return after 250ms
      return $q(function(resolve) {
        setTimeout(function() {
            resolve(logs);
        }, 250);
      });

      // return $http.get('http://localhost:8080/sql/log/get');
    },

    /**
     * Returns a page of log entries
     *
     * @param page the page number to retrieve
     * @returns {HttpPromise} a HTTP promise with the page of log entries
     */
    getPage: function (page) {
      return $http.get('http://localhost:8080/sql/log/page');
    }
  };
}]);