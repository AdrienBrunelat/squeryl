/**
 * Component used to display the logs.
 *
 * @author Adrien Brunelat [adrien.brunelat@gmail.com]
 * @since 24/06/17
 */
'use strict';

angular.module('squerylApp')

.component('logs', {
  templateUrl: 'components/logs/logs.component.html',
  controller: 'LogsCtrl'
})

.controller('LogsCtrl', ['$scope', 'logService', function ($scope, logService) {
  $scope.logs = [];

  $scope.httpMethods = {
    GET: {name: 'GET', included: true},
    POST: {name: 'POST', included: true},
    PUT: {name: 'PUT', included: true},
    PATCH: {name: 'PATCH', included: true},
    DELETE: {name: 'DELETE', included: true}
  };

  $scope.updateList = function () {
    // $scope.logs = [];
    logService.getAll().then(
      function success(response) {
        response.forEach(function (log) {
          if ($scope.httpMethods[log.requestType] && $scope.httpMethods[log.requestType].included) {
            $scope.logs.push(log);
          }
        });
      },
      function error(response) {
        // Eventually code a behavior corresponding the error case here
      }
    );
  };

  $scope.updateList();
}]);
