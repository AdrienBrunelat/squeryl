'use strict';

angular.module('squerylApp.version.interpolate-filter', [])

.filter('interpolate', ['version', function (version) {
  return function (text) {
    return String(text).replace(/\%VERSION\%/mg, version);
  };
}]);
