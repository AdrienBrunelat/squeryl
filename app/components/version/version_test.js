'use strict';

describe('squerylApp.version module', function () {
  beforeEach(module('squerylApp.version'));

  describe('version service', function () {
    it('should return current version', inject(function (version) {
      expect(version).toEqual('0.1');
    }));
  });
});
