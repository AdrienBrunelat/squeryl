'use strict';

angular.module('squerylApp.version', [
  'squerylApp.version.interpolate-filter',
  'squerylApp.version.version-directive'
])

.value('version', '0.1');
